hijra (1.0-1) unstable; urgency=medium

  * New upstream version 1.0
  * Drop py3.diff patch, applied upstream

 -- أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@users.sourceforge.net>  Fri, 17 Jan 2020 15:54:52 +0200

hijra (0.4.1-5) unstable; urgency=medium

  * Drop XS-Autobuild field, as autobuild request got rejected

 -- أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@users.sourceforge.net>  Fri, 10 Jan 2020 03:26:16 +0100

hijra (0.4.1-4) unstable; urgency=medium

  * Update py3.diff from upstream
  * Set debhelper compat level in build deps
  * Update copyright years

 -- أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@users.sourceforge.net>  Wed, 08 Jan 2020 03:17:08 +0100

hijra (0.4.1-3) unstable; urgency=medium

  * Add Breaks+Replaces: python-hijra (Closes: #944792)
  * Fix indentation in py3.diff patch (Closes: #944767)
  * Drop gnome-shell upper limit dependency (Closes: #944768)
  * Add gitlab-ci.yml

 -- أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@users.sourceforge.net>  Sun, 08 Dec 2019 04:08:51 +0100

hijra (0.4.1-2) unstable; urgency=medium

  * Split GNOME shell extension into a separate package (Closes: #802512)
  * Migrate to Python 3 (Closes: #936703)
    + Added py3.diff patch to migrate to Python 3
  * Bumped to compat level 12
  * d/control:
    + Update Vcs-* fields
    + Add dh-python to build-deps
    + Use Breaks instead of Conflicts
    + Update to standards version 4.4.0
    + Replaced python-gtk dependency with python3-gi (Closes: #885305)
    + Update standards version to 4.4.1
    + Add Rules-Requires-Root: no
    + Add XS-Autobuild: yes
  * d/copyright:
    + Switch to secure copyright format URI
    + Add explanation for non-free package
    + Update copyright years
    + Switch to WPL-2 license
  * Add upstream metadata
  * Update homepage URL

 -- أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@users.sourceforge.net>  Sun, 03 Nov 2019 05:08:36 +0100

hijra (0.4.1-1) unstable; urgency=medium

  * New upstream release (Closes: #799441)
  * Change priority to optional
  * Update URL in watch file
  * Update my email address
  * Change Vcs-* fields to secure canonical URLs
  * Remove obsolete DMUA field
  * Update Standards-Version to 4.1.1
  * Bumped compat level to 10
  * debian/copyright:
    + Update copyright years
    + Switch to DEP-5 copyright format
  * Remove menu file
  * Removed shell_3.2.x_compat.diff patch, no longer needed
  * Fix capitalization error in python-hijra description

 -- أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@users.sourceforge.net>  Thu, 12 Oct 2017 17:33:42 +0200

hijra (0.2.1-1) unstable; urgency=low

  * New upstream release.
  * Dropped patch: gnome-shell_3.2.diff
  * Bumped compat level to 9.
  * debian/copyright: Updated copyright years.
  * Added shell_3.2.x_compat.diff patch to make GNOME shell extension
    compatible with GNOME shell 3.2.x

 -- أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@sabily.org>  Thu, 16 Feb 2012 16:05:11 +0200

hijra (0.2.0-2) unstable; urgency=low

  * Added gnome-shell_3.2.diff patch to update gnome-shell extension to be
    compatible with Gnome shell 3.2
  * debian/copyright: Updated copyright format & years

 -- أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@sabily.org>  Tue, 01 Nov 2011 17:38:36 +0200

hijra (0.2.0-1) unstable; urgency=low

  [ أحمد المحمودي (Ahmed El-Mahmoudy) ]
  * New upstream release.
  * debian/copyright:
    + Replace empty lines in license texts with " ."
    + Change license of debian packaging to GPL-3+ or Waqf Public License
      to avoid incompatible license issues.
  * Switch to dh_python2
    + debian/control:
      - Drop python-support from Build-Deps
      - Bumped python-all Build-Dep to (>= 2.6.6-3~)
      - Used X-Python-Version control field instead of debian/pyversions
      - Remove XB-Python-Version fields
      - Remove Provides: ${python:Provides}
    + debian/rules: added --with python2 to dh call.
  * Bumped compat level to 8
  * debian/control:
    + Bumped Standards-Version to 3.9.2
    + Removed python-eggtrayicon from hijra-applet Depends
    + Updated hijra-applet's description.
  * debian/rules: No more need for dh_auto_configure & dh_clean overrides
  * debian/hijra-applet.install: Added tray icon template & GNOME shell
    extension
  * Refreshed setup_debian.diff patch.
  * Removed desktop.diff patch, as it got applied upstream.

  [ Mehdi Dogguy ]
  * Add DM-Upload-Allowed bit in control file.

 -- أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@sabily.org>  Thu, 28 Jul 2011 17:44:08 +0200

hijra (0.1.18-3) unstable; urgency=low

  * Team upload.

  [ Mehdi Dogguy ]
  * hijra is for non-free (not main).

 -- Debian Islamic Maintainers <debian-islamic-maintainers@lists.alioth.debian.org>  Tue, 06 Jul 2010 17:53:13 +0200

hijra (0.1.18-2) unstable; urgency=low

  Team upload.

  [ Mehdi Dogguy ]
  * Fix maintainer's address.

 -- Debian Islamic Maintainers <debian-islamic-maintainers@lists.alioth.debian.org>  Sat, 03 Jul 2010 12:30:50 +0200

hijra (0.1.18-1) unstable; urgency=low

  * Initial release (Closes: #513281)
  * Added desktop.patch to:
    + Remove Encoding field.
    + Add X-Islamic-Software category.
    + Replace irrevelant categories with Utility.
  * Added patch setup_debian.diff to install HijriApplet as a script,
    and install documentation in /usr/share/doc/python-hijra

 -- أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@sabily.org>  Mon, 28 Jun 2010 18:39:39 +0300
